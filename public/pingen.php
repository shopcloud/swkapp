<?php


$sToken = '4f72253aa51b3e6d06e2863b91ea4961'; //Add your private token here
$sFile = realpath('example.pdf');
echo $sFile;
//set URL
$sURL = 'https://stage.api.pingen.com/document/upload/token/' . $sToken;
#$sURL = 'https://api.pingen.com/document/upload/token/' . $sToken;

//build our query data
$aData = array(
'data' => json_encode(array(
'send' => true, //we want to automatically send it
'speed' => 1, //we want to send priority
'color' => 1, //and in color
)),
'file' => '@' . $sFile
);

//set the url, number of POST vars, POST data
$objCurlConn = curl_init();
curl_setopt($objCurlConn,CURLOPT_URL,$sURL);
curl_setopt($objCurlConn,CURLOPT_POST,1);
curl_setopt($objCurlConn,CURLOPT_POSTFIELDS,$aData);
curl_setopt($objCurlConn, CURLOPT_RETURNTRANSFER, 1);
curl_setopt ($objCurlConn, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt ($objCurlConn, CURLOPT_SSL_VERIFYPEER, 0);

//execute post
$sResult = curl_exec($objCurlConn);

// decode result and pass from here on
$aResult = json_decode($sResult, true);

echo "$sURL <br>";

if ($aResult['error']==true)
{
    echo "error";
    print_r($aResult);
//an error occured, handle it from here
}else {
//go along with your code here

    echo "success";
}