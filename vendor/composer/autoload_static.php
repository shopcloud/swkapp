<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit106229545d11dfb69804c6aebb0ec9cc
{
    public static $prefixLengthsPsr4 = array (
        'p' => 
        array (
            'pingencom\\' => 10,
        ),
        'm' => 
        array (
            'mikehaertl\\wkhtmlto\\' => 20,
            'mikehaertl\\tmp\\' => 15,
            'mikehaertl\\shellcommand\\' => 24,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'pingencom\\' => 
        array (
            0 => __DIR__ . '/..' . '/pingencom/pingen/src',
        ),
        'mikehaertl\\wkhtmlto\\' => 
        array (
            0 => __DIR__ . '/..' . '/mikehaertl/phpwkhtmltopdf/src',
        ),
        'mikehaertl\\tmp\\' => 
        array (
            0 => __DIR__ . '/..' . '/mikehaertl/php-tmpfile/src',
        ),
        'mikehaertl\\shellcommand\\' => 
        array (
            0 => __DIR__ . '/..' . '/mikehaertl/php-shellcommand/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit106229545d11dfb69804c6aebb0ec9cc::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit106229545d11dfb69804c6aebb0ec9cc::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
