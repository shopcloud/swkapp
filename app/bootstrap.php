<?php

class bootstrap
{
    public function connectDb()
    {

        define("LIB_DIR", "../library/");
        $include_path = get_include_path();

        if (!ini_set('include_path', $include_path . PATH_SEPARATOR . LIB_DIR)) {
            die('Failed to set the include path. Check you php configuration.');
        }

        include "Zend/Loader.php";

        Zend_Loader::loadClass('Zend_Db');
        Zend_Loader::loadClass('Zend_Config_Ini');

        try {
            $zdb = Zend_Db::factory('Pdo_Mysql', array(
                'host' => DBHOST,
                'username' => DBUSER,
                'password' => DBPASS,
                'dbname' => DBNAME
            ));
            $zdb->getConnection();
        } catch (Zend_Db_Exception $e) {
            die("Can't access the DB" . $e->getMessage());
        }

        $zdb->setFetchMode(Zend_Db::FETCH_ASSOC);
        $zdb->query('SET NAMES utf8');

        #$zdb->setFetchMode(Zend_Db::FETCH_OBJ);
        $this->db = $zdb;

    }


    /**
     * Translate a result array into a HTML table
     *
     * @author      Aidan Lister <aidan@php.net>
     * @version     1.3.1
     * @link        http://aidanlister.com/repos/v/function.array2table.php
     * @param       array $array The result (numericaly keyed, associative inner) array.
     * @param       bool $recursive Recursively generate tables for multi-dimensional arrays
     * @param       bool $return return or echo the data
     * @param       string $null String to output for blank cells
     */
    function array2table($array, $recursive = false, $return = true, $null = '&nbsp;')
    {
        // Sanity check
        if (empty($array) || !is_array($array)) {
            return false;
        }

        if (!isset($array[0]) || !is_array($array[0])) {
            $array = array($array);
        }

        // Start the table
        $table = "<table class=\"table table-striped \">\n";

        // The header
        $table .= "\t<tr>";
        // Take the keys from the first row as the headings
        foreach (array_keys($array[0]) as $heading) {
            $table .= '<th>' . $heading . '</th>';
        }
        $table .= "</tr>\n";

        // The body
        foreach ($array as $row) {
            $table .= "\t<tr>";
            foreach ($row as $cell) {
                $table .= '<td>';

                // Cast objects
                if (is_object($cell)) {
                    $cell = (array)$cell;
                }

                if ($recursive === true && is_array($cell) && !empty($cell)) {
                    // Recursive mode
                    $table .= "\n" . array2table($cell, true, true) . "\n";
                } else {
                    $table .= (strlen($cell) > 0) ?
                        #htmlspecialchars((string)$cell) :
                        ($cell) :
                        $null;
                }

                $table .= '</td>';
            }

            $table .= "</tr>\n";
        }

        // End the table
        $table .= '</table>';

        // Method of output
        if ($return === false) {
            echo $table;
        } else {
            return $table;
        }
    }


    function array2htmltable($arr){

        //$arr = (object) $arr; )
        $code = "<table class=\"table table-striped \">\n";
        if ($arr) {
            foreach ($arr as $key =>$row) {
                $code .= "<tr>";
                foreach ($row as $key2 =>$colum) {
                    $code .= "<td>".($colum)."</td>";
                }
            }
        }
        $code .= '</table>';

        //echo $code;
        // print_r($arr);

        return $code;

    }

    public function helperRenderLayout($content, $title)
    {
        $code = '
        
        <!doctype html>
<html lang="de">

<head>

<meta http-equiv="content-type" content="text/html; charset=utf-8">

    <link rel="stylesheet" type="text/css" media="all" href="https://428948.shops.s-media.dev/static/version1598606284/frontend/SME/mella/de_DE/ECG_Base/library/bootstrap4.0/css/bootstrap.min.css" />
</head>


<body>

<style>
    html {
        font-size: 0.8rem;
    }

    body {
        padding: 20px;
    }

    textarea {
        background-color: #f3f3f3;
    }
</style>

<div class="container-fluid mb-3">

<h1>'.$title.'</h1>

</div>

<div class="container-fluid">


'
            . $content .

            '            

</div>

</body>

</html>
        
        ';

        return $code;
    }


}