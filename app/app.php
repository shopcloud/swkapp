<?php

require __DIR__ . '/../vendor/autoload.php';

use pingencom\Pingen;
use mikehaertl\wkhtmlto\Pdf;

class app extends bootstrap
{
    const TEMP_FOLDER = __DIR__ . '/../data/tmp/';
    const TEMPLATES_FOLDER = __DIR__ . '/../public/templates/';

    #const PINGEN_TOKEN = '2b5e22b8b22886fe7950ba034a1daf7a'; //info@medientech.net
    const PINGEN_TOKEN = '4f72253aa51b3e6d06e2863b91ea4961'; //volker.scharnagl@medientech.net // SWKK

    const COUNTRIES_CODES = ['CH' => 'Schweiz'];

    const EMAIL_WEBMASTER = 'sslmedia@gmail.com';

    const SALUTATIONS = ['M' => 'Lieber Herr', 'F' => 'Liebe Frau'];

    const WEBTOKEN = '92739089w8fzej23uzfejzejk';

    function __construct()
    {
        define('DBHOST', 'localhost');
        #define('DBUSER', 'root');
        #define('DBPASS', 'asdfakZZsKK39skdB323922');
        define('DBUSER', 'swkapp');
        define('DBPASS', 'wQ9yRBwhR');
        define('DBNAME', 'swkapp');
        $this->connectDb();
    }


    public function run($request)
    {
        $action = $request['action'];
        $token = $request['token'];

        if ($action == '') {
            echo 'no action.';
            die;
        }

        if ($token != self::WEBTOKEN) {
            echo 'token not correct.';
            die;
        }

        switch ($action) {
            case "run":
                $this->process();
                break;
            case "cron":
                $this->cron();
                break;
            case "senddemopdf":
                $this->senddemopdf();
                break;
            case "viewdemopdf":
                $this->viewdemopdf();
                break;
            case "viewcurrentaddresses":
                $this->viewcurrentaddresses();
                break;
            default:
                echo 'Action not found';
                die;
                break;
        }

    }

    public function process()
    {
        // gets
        $addresses = $this->getCurrentAddresses();
        if (!empty($addresses)) {
            foreach ($addresses as $aKey => $address) {
                $outputPDF = $this->generatePDF($address, false);
                if (!empty($outputPDF)) {
                    $address['outputPDF'] = $outputPDF;
                    $this->sendPDF($address, true, false);
                    #return;
                    #die(); // fixme
                } else {
                    echo $address['id'] . " - PDF konnte nicht erstellt werden.<br>";
                }
            }
        }
    }

    private function viewcurrentaddresses()
    {
        $addresses = $this->getCurrentAddresses();
        echo $this->helperRenderLayout($this->array2table($addresses), 'Versand an folgende Adressen');
        #echo $this->helperRenderLayout($this->array2htmltable($addresses), 'Versand erfolgt heute an folgende Adressen:');
        #echo "<pre>"; print_r($addresses);
    }

    private function senddemopdf()
    {
        // gets
        $addresses = $this->getCurrentAddresses();
        $addresses = [0 => [
            'id' => 1,
            'street' => 'Kantonsstrasse 13',
            'zip' => '8862',
            'city' => 'Schübelbach',
            'country' => 'CH',
            'firstname' => '',
            'lastname' => 'Scharnagl',
            'gender' => 'M'
        ]];
        if (!empty($addresses)) {
            foreach ($addresses as $aKey => $address) {
                $outputPDF = $this->generatePDF($address, false);
                if (!empty($outputPDF)) {
                    $address['outputPDF'] = $outputPDF;
                    #$this->sendPDF($address, false,true);
                    $this->sendPDF($address, true, false);
                    die();
                } else {
                    echo $address['id'] . " - PDF konnte nicht erstellt werden.<br>";
                }
            }
        }
    }


    private function viewdemopdf()
    {
       
	#echo "action: viewdemopdf";

        // gets
        $addresses = $this->getCurrentAddresses();
        $addresses = [0 => [
            'id' => 1,
            'street' => 'Kantonsstrasse 13',
            'zip' => '8862',
            'city' => 'Schübelbach',
            'country' => 'CH',
            'firstname' => '',
            'lastname' => 'Scharnagl',
            'gender' => 'M'
        ]];
        if (!empty($addresses)) {
            foreach ($addresses as $aKey => $address) {
                $outputPDF = $this->generatePDF($address, true);
                die;
            }
        }
    }

    public function rundemo()
    {
        echo "action: rundemo";

    }

    private function sendPDF($address, $sendPDF = true, $demo = false)
    {
        try {

            $this->log('info', 'start', $address['id']);
            #print_r($address); die;

            $pingen = new Pingen(self::PINGEN_TOKEN, $demo ? Pingen::MODE_STAGING : Pingen::MODE_PRODUCTION);

            $uploadResponse = $pingen->document_upload($address['outputPDF']);

            $documentId = $uploadResponse->id;

            if ($sendPDF) {
                $sendResponse = $pingen->document_send($documentId, Pingen::SPEED_PRIORITY, Pingen::PRINT_COLOR);
                $sendId = $sendResponse->id;
                $sendObject = $pingen->send_get($sendId);
                $statusCode = $sendObject->item->status;

                $message = "sendId '$sendId' statusCode '$statusCode' token " . substr(self::PINGEN_TOKEN, 0, 10) . '... ' . $address['firstname'] . ' ' . $address['lastname'];
                $this->log('success', $message, $address['id']);
                echo "<p>$message</p>";
                // save setNextBirthday
                $this->setNextBirthday($address);
            }

            echo "Success" . PHP_EOL;

        } catch (Exception $e) {
            $message = 'An error with address: ' . $address['id'] . ' occured with number <b>' . $e->getCode() . '</b> and message <b>' . $e->getMessage() . '</b>' . PHP_EOL;
            echo $message;
            $this->log('error', $message, $address['id']);
        }
    }


    private function setNextBirthday($address)
    {
        $date = $address['next_birthday_date'];
        $date = strtotime($date);
        $new_date = strtotime('+ 1 year', $date);
        $new_next_birthday_date = date('Y-m-d', $new_date);
        $arr = array(
            'next_birthday_date' => $new_next_birthday_date
        );
        $this->db->update('addresses', $arr, "id = " . $address['id']);
        #print_r($address); die;
    }

    private function log($type, $message, $addresses_id = false)
    {
        $arr = array(
            'addresses_id' => $addresses_id,
            'created_time' => date("Y-m-d H:i:s"),
            'modified_time' => date("Y-m-d H:i:s"),
            'type' => $type,
            'message' => $message,
        );
        $this->db->insert('log_mail', $arr);

        if ($type == 'error') {
            mail(self::EMAIL_WEBMASTER, 'SWK APP Error', print_r($arr, true));
        }
    }


    private function generatePDF(array $address, $showInBrowser = false): ?string
    {

        $filePath = self::TEMP_FOLDER . 'birthdayCard_' . date('Ymdhis') . '_' . $address['id'] . '.pdf';

        $pdf = new Pdf;

        $options = array(
            'page-width' => '210mm',
            'page-height' => '297mm',
            'dpi' => 300,
            'image-quality' => 100,
            'margin-right' => '5mm',
            'margin-bottom' => '5mm',
            'margin-top' => '5mm',
            'margin-left' => '5mm',
        );
        $pdf->setOptions($options);

        $content = "";
        ob_start();
        $country = $this->getCountryNameByISO2($address['country']);
        include self::TEMPLATES_FOLDER . "letterTemplate.html";
        $content = ob_get_clean();

        $pdf->addPage($content);

        if (!$pdf->saveAs($filePath)) {  //todo save filename
            $error = $pdf->getError();
            return null;
        }
        if ($showInBrowser) {
            header("Content-type:application/pdf");
            $pdf->send();
        }
        return $filePath;
    }

    private function getCountryNameByISO2($countryName)
    {
        return self::COUNTRIES_CODES[$countryName];
    }

    private function getCurrentAddresses(): ?array
    {

        $timestamp = time();
        $weekdays = array("Sunday", "Monday", "Tuesday",
            "Wednesday", "Thursday", "Friday", "Saturday");
        $weekday_today = $weekdays[date("w", $timestamp)];

        //$weekday_today = 'Friday';

        if ($weekday_today == 'Friday') {
            // am freitag 3 tage
            $date_sql = 'AND ( 
                            DATE(next_birthday_date) = DATE(NOW() + INTERVAL 1 DAY)
                            OR DATE(next_birthday_date) = DATE(NOW() + INTERVAL 2 DAY)
                            OR DATE(next_birthday_date) = DATE(NOW() + INTERVAL 3 DAY)
                            )';
        } elseif ($weekday_today == 'Saturday' OR $weekday_today == 'Sunday') {
            /// kein ausfuehren am sa und so
            echo "on $weekday_today no adresses";
            die;
        } else {
            $date_sql = 'AND DATE(next_birthday_date) = DATE(NOW() + INTERVAL 1 DAY)';
        }

        $db = $this->db;
        /*
        $query = "SELECT * from addresses WHERE 
                      status = 'Aktiv' 
                      AND (last_contact_date LIKE '%2019' OR last_contact_date LIKE '%2020')  
                      AND lastname <> ''
                      AND street <> ''
                      AND zip <> ''
                      AND city <> ''
                      $date_sql
                      ";
        */
        $query1 = "SELECT * from addresses WHERE 
                      status = 'Aktiv' 
                      AND (last_contact_date LIKE '%2020')  
                      AND lastname <> ''
                      AND street <> ''
                      AND zip <> ''
                      AND city <> ''
                      $date_sql
                      ";
      $query = "SELECT * from addresses WHERE 
                      status = 'Aktiv' 
                    
                      AND lastname <> ''
                      AND street <> ''
                      AND zip <> ''
                      AND city <> ''
                      $date_sql
                      ";
		
        #echo $query; //todo debug
        $result = $db->fetchall($query);

        #print_r($result);

        if ($result) {
            return $result;
        };
        return null;
    }

    public function cron()
    {
        // log cronjob startet
        $this->log('info', 'cronjob start');

        // run
        $this->process();

        // log conrjob endet
        $this->log('info', 'cronjob end');
    }


}


class swkapp extends app
{

}
